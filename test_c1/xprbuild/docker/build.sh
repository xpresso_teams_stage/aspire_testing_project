#! /bin/bash
## This script is used to build the docker image for the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

DOCKER_IMAGE_NAME=${1}
TAG=${2}
current_folder=${ROOT_FOLDER}/xprbuild/docker

docker_registry=$(echo $DOCKER_IMAGE_NAME | cut -f1 -d/)
if [[ ! -z "$DOCKER_IMAGE_NAME" ]]
then
	docker login ${docker_registry} -u admin -p Abz00ba@123
fi

cmd="docker build -t ${DOCKER_IMAGE_NAME}:${TAG} -f $current_folder/Dockerfile ${ROOT_FOLDER}"
echo "Building the docker image"
echo "Docker build command -> $cmd"
exec $cmd

