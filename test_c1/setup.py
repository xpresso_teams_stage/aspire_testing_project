from setuptools import setup

with open('requirements/requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

version_file_name = 'VERSION'
version_fs = open(version_file_name)
version = version_fs.read().strip()
version_fs.close()

# Setup configuration
setup(
    name='test_c1',
    version=version,
    packages=['test_c1'],
    description="Sample library code",
    install_requires=requirements
)